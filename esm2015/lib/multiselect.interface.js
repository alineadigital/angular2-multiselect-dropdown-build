/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function DropdownSettings() { }
/** @type {?} */
DropdownSettings.prototype.singleSelection;
/** @type {?} */
DropdownSettings.prototype.text;
/** @type {?} */
DropdownSettings.prototype.enableCheckAll;
/** @type {?} */
DropdownSettings.prototype.selectAllText;
/** @type {?} */
DropdownSettings.prototype.unSelectAllText;
/** @type {?} */
DropdownSettings.prototype.filterSelectAllText;
/** @type {?} */
DropdownSettings.prototype.filterUnSelectAllText;
/** @type {?} */
DropdownSettings.prototype.enableFilterSelectAll;
/** @type {?} */
DropdownSettings.prototype.enableSearchFilter;
/** @type {?} */
DropdownSettings.prototype.searchBy;
/** @type {?} */
DropdownSettings.prototype.maxHeight;
/** @type {?} */
DropdownSettings.prototype.badgeShowLimit;
/** @type {?} */
DropdownSettings.prototype.classes;
/** @type {?|undefined} */
DropdownSettings.prototype.limitSelection;
/** @type {?|undefined} */
DropdownSettings.prototype.disabled;
/** @type {?} */
DropdownSettings.prototype.searchPlaceholderText;
/** @type {?|undefined} */
DropdownSettings.prototype.groupBy;
/** @type {?|undefined} */
DropdownSettings.prototype.showCheckbox;
/** @type {?} */
DropdownSettings.prototype.noDataLabel;
/** @type {?|undefined} */
DropdownSettings.prototype.searchAutofocus;
/** @type {?|undefined} */
DropdownSettings.prototype.lazyLoading;
/** @type {?|undefined} */
DropdownSettings.prototype.labelKey;
/** @type {?} */
DropdownSettings.prototype.primaryKey;
/** @type {?} */
DropdownSettings.prototype.position;
/** @type {?|undefined} */
DropdownSettings.prototype.loading;
/** @type {?|undefined} */
DropdownSettings.prototype.selectGroup;
/** @type {?|undefined} */
DropdownSettings.prototype.addNewItemOnFilter;
/** @type {?} */
DropdownSettings.prototype.addNewButtonText;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXVsdGlzZWxlY3QuaW50ZXJmYWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhcjItbXVsdGlzZWxlY3QtZHJvcGRvd24vIiwic291cmNlcyI6WyJsaWIvbXVsdGlzZWxlY3QuaW50ZXJmYWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIERyb3Bkb3duU2V0dGluZ3N7XG4gICAgc2luZ2xlU2VsZWN0aW9uOiBib29sZWFuO1xuICAgIHRleHQ6IHN0cmluZztcbiAgICBlbmFibGVDaGVja0FsbCA6IGJvb2xlYW47XG4gICAgc2VsZWN0QWxsVGV4dDogc3RyaW5nO1xuICAgIHVuU2VsZWN0QWxsVGV4dDogc3RyaW5nO1xuICAgIGZpbHRlclNlbGVjdEFsbFRleHQ6IHN0cmluZztcbiAgICBmaWx0ZXJVblNlbGVjdEFsbFRleHQ6IHN0cmluZztcbiAgICBlbmFibGVGaWx0ZXJTZWxlY3RBbGw6IGJvb2xlYW47XG4gICAgZW5hYmxlU2VhcmNoRmlsdGVyOiBib29sZWFuO1xuICAgIHNlYXJjaEJ5OiBBcnJheTxzdHJpbmc+W107XG4gICAgbWF4SGVpZ2h0OiBudW1iZXI7XG4gICAgYmFkZ2VTaG93TGltaXQ6IG51bWJlcjtcbiAgICBjbGFzc2VzOiBzdHJpbmc7XG4gICAgbGltaXRTZWxlY3Rpb24/OiBudW1iZXI7XG4gICAgZGlzYWJsZWQ/OiBib29sZWFuO1xuICAgIHNlYXJjaFBsYWNlaG9sZGVyVGV4dDogc3RyaW5nO1xuICAgIGdyb3VwQnk/OiBzdHJpbmc7XG4gICAgc2hvd0NoZWNrYm94PzogYm9vbGVhbjtcbiAgICBub0RhdGFMYWJlbDogc3RyaW5nO1xuICAgIHNlYXJjaEF1dG9mb2N1cz86IGJvb2xlYW47XG4gICAgbGF6eUxvYWRpbmc/OiBib29sZWFuO1xuICAgIGxhYmVsS2V5Pzogc3RyaW5nO1xuICAgIHByaW1hcnlLZXk6IHN0cmluZztcbiAgICBwb3NpdGlvbjpzdHJpbmc7XG4gICAgbG9hZGluZz86IGJvb2xlYW47XG4gICAgc2VsZWN0R3JvdXA/OiBib29sZWFuO1xuICAgIGFkZE5ld0l0ZW1PbkZpbHRlcj86IGJvb2xlYW47XG4gICAgYWRkTmV3QnV0dG9uVGV4dDogc3RyaW5nO1xufSBcbiJdfQ==