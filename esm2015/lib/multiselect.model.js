/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
export class MyException {
    /**
     * @param {?} status
     * @param {?} body
     */
    constructor(status, body) {
        this.status = status;
        this.body = body;
    }
}
if (false) {
    /** @type {?} */
    MyException.prototype.status;
    /** @type {?} */
    MyException.prototype.body;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXVsdGlzZWxlY3QubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hbmd1bGFyMi1tdWx0aXNlbGVjdC1kcm9wZG93bi8iLCJzb3VyY2VzIjpbImxpYi9tdWx0aXNlbGVjdC5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsTUFBTTs7Ozs7SUFHTCxZQUFZLE1BQWUsRUFBRSxJQUFVO1FBQ3RDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO0tBQ2pCO0NBRUQiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgTXlFeGNlcHRpb24ge1xuXHRzdGF0dXMgOiBudW1iZXI7XG5cdGJvZHkgOiBhbnk7XG5cdGNvbnN0cnVjdG9yKHN0YXR1cyA6IG51bWJlciwgYm9keSA6IGFueSkge1xuXHRcdHRoaXMuc3RhdHVzID0gc3RhdHVzO1xuXHRcdHRoaXMuYm9keSA9IGJvZHk7XG5cdH1cblx0XG59Il19