/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var MyException = /** @class */ (function () {
    function MyException(status, body) {
        this.status = status;
        this.body = body;
    }
    return MyException;
}());
export { MyException };
if (false) {
    /** @type {?} */
    MyException.prototype.status;
    /** @type {?} */
    MyException.prototype.body;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXVsdGlzZWxlY3QubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hbmd1bGFyMi1tdWx0aXNlbGVjdC1kcm9wZG93bi8iLCJzb3VyY2VzIjpbImxpYi9tdWx0aXNlbGVjdC5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsSUFBQTtJQUdDLHFCQUFZLE1BQWUsRUFBRSxJQUFVO1FBQ3RDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO0tBQ2pCO3NCQU5GO0lBUUMsQ0FBQTtBQVJELHVCQVFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIE15RXhjZXB0aW9uIHtcblx0c3RhdHVzIDogbnVtYmVyO1xuXHRib2R5IDogYW55O1xuXHRjb25zdHJ1Y3RvcihzdGF0dXMgOiBudW1iZXIsIGJvZHkgOiBhbnkpIHtcblx0XHR0aGlzLnN0YXR1cyA9IHN0YXR1cztcblx0XHR0aGlzLmJvZHkgPSBib2R5O1xuXHR9XG5cdFxufSJdfQ==